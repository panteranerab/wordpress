<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ultetriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'wordpress');

/** Nome utente del database MySQL */
define('DB_USER', 'ifts');

/** Password del database MySQL */
define('DB_PASSWORD', 'ifts');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8');

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sPR(!WXmO?XMfCJmP}Z1GbSzL~!Wt5-2m4SeL?d-_U[`ue7(=me2!Y)GmlvuPC}}');
define('SECURE_AUTH_KEY',  '-X${Y?+ZnZntsv/aF12}9tG`,%?0!`=lShiV7@ORM |c~fUXX;{-#qY=5Gi&<~c,');
define('LOGGED_IN_KEY',    '%4VkI-pz!c%, 0 0JLT7P#|h&+f30R^~$D$Bk-KZCU~G+kVsk=*Lfu[Gp.WV^JGS');
define('NONCE_KEY',        '}sk.8ETV%ZNMp9hJ.Mv`:AQr!Z&!,<qR|=Dz4;g{S>pY-MWi6)wjb.=zS#>o~p-G');
define('AUTH_SALT',        'gpKdb1gyn&z3Mhl-+|--I$?naE@y;G?>cSb5.w:Z9c9Izh[$oRj86:5q69jdP>BQ');
define('SECURE_AUTH_SALT', 'Tj?hCJ1aC*nx?1}OU%x tI <hS<|o&M-L5&[.$R}gF!;I(#CP]O2vjkzVxJvyR6*');
define('LOGGED_IN_SALT',   'N55Psm(3W(+b^K37`D4bdGlP|:9fD 2Wr14kn7r{T5tf@r+7A-v-L-c_z3@D9O?J');
define('NONCE_SALT',       '1 omxs_-f.|mk,ItlnYL*>+ImY:&u8`S-96.];i(nG8<gd1f9&s#@Lx+EOO OE4F');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');